package eu.mirkodi.swatchbeatclock;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Objects;
import java.util.TimeZone;

public class InternetTime {
    // settings
    static final int MAXTIMEARRELEMS = 6; // hour, min, sec, millis, .... MAXTIMEARRELEMS

    static final String TIMEZONEID = "GMT+1"; /* > CET observes daylight saving, so it's not good
                                               > UTC+1 doesn't work (getTimeZone() doesn't support it
                                               > GMT+1 just werks
                                             */
    public static final TimeZone TIMEZONE = TimeZone.getTimeZone(TIMEZONEID);

    private static final float MILLIS_IN_A_SECOND = 1000f,
            SECONDS_IN_A_MINUTE = 60f,
            MINUTES_IN_AN_HOUR = 60f;


    private final double initial; // set when obj is created

    private final static DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();

    // initialise with current time
    public InternetTime() {
        initial = hoursToBeat(getCurrentTimeHours());
    }

    // @return decimal hour value
    public static double getCurrentTimeHours() {
        Calendar cal = Calendar.getInstance(TIMEZONE);
        return timeToDoubleHours(TimeUtils.getCurrentHour(cal), TimeUtils.getCurrentMinute(cal),
                TimeUtils.getCurrentSecond(cal), TimeUtils.getCurrentMillisecond(cal));
    }

    public static double hoursToBeat(double hours) {
        // 24:hours=1000:x
        return (hours * 1000) / 24;
    }

    private static double timeToDoubleHours(int hour, int minute, int second, int millis) {
        return hour + ((minute + ((second + (millis / MILLIS_IN_A_SECOND))
                / SECONDS_IN_A_MINUTE)) / MINUTES_IN_AN_HOUR);
    }

    // @return: [0]: hour; [1]: minute; [2]: seconds; [3]: millis......
    private static int[] doubleHoursToTime(double hours) {
        int[] ret = new int[MAXTIMEARRELEMS];

        double n = hours;
        int i = 0;
        do {
            int intN = (int) n;
            ret[i] = intN;

            double m = n - intN;
            if (i < 2) {
                n = m * 60;
            } else {
                n = m * 1000;
            }

            i++;
        }
        while (n - ((int) n) != 0 && i < MAXTIMEARRELEMS);

        return ret;
    }

    public static double timeToBeat(int hour, int minute, int seconds, int millis) {
        return sanitiseBeats(hoursToBeat(timeToDoubleHours(hour, minute, seconds, millis)));
    }

    public static double beatToDoubleHours(double beats) {
        return (beats * 24) / 1000 /* 1000:beats=24:hours */;
    }

    // @return: [0]: hour; [1]: minute; [2]: seconds; [3]: millis
    public static int[] beatToTime(String beats) throws ParseException {
        if (beats.isEmpty()) {
            return null;
        }
        return beatToTime(Objects.requireNonNull(df.parse(beats)).doubleValue());
    }

    // @return: [0]: hour; [1]: minute; [2]: seconds; [3]: millis
    public static int[] beatToTime(double beats) {
        return doubleHoursToTime(beatToDoubleHours(sanitiseBeats(beats)));
    }

    public static double getCurrentTimeBeats() {
        return hoursToBeat(getCurrentTimeHours());
    }

    // returns time saved when creating the class instance
    public double getInitial() {
        return initial;
    }

    public static double sanitiseBeats(double beats) {
        return TimeUtils.sanitise(beats, 0, 1000);
    }
}