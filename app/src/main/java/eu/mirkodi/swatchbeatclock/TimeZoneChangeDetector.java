package eu.mirkodi.swatchbeatclock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Objects;

public class TimeZoneChangeDetector extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // ensure that what changed is actually the TimeZone
        if (Objects.equals(intent.getAction(), Intent.ACTION_TIMEZONE_CHANGED)) {
            ConverterFragment.timeZoneChanged();
        }
    }
}