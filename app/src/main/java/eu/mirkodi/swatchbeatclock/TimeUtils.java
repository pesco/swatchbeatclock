package eu.mirkodi.swatchbeatclock;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class TimeUtils {
    public static final int HOUR_GET_CODE = 0, MINUTE_GET_CODE = 1, SECOND_GET_CODE = 2, MILLISECOND_GET_CODE = 3;

    private static final Map<TimeZone, Calendar> TZToC = new HashMap<>();

    public static int getCurrentHour(TimeZone tz) {
        return get(HOUR_GET_CODE, tz);
    }

    public static int getCurrentMinute(TimeZone tz) {
        return get(MINUTE_GET_CODE, tz);
    }

    public static int getCurrentSecond(TimeZone tz) {
        return get(SECOND_GET_CODE, tz);
    }

    public static int getCurrentMillisecond(TimeZone tz) {
        return get(MILLISECOND_GET_CODE, tz);
    }

    public static int getCurrentHour(Calendar cal) {
        return get(HOUR_GET_CODE, cal);
    }

    public static int getCurrentMinute(Calendar cal) {
        return get(MINUTE_GET_CODE, cal);
    }

    public static int getCurrentSecond(Calendar cal) {
        return get(SECOND_GET_CODE, cal);
    }

    public static int getCurrentMillisecond(Calendar cal) {
        return get(MILLISECOND_GET_CODE, cal);
    }

    /**
     * @param getCode wants *_GET_CODE constants. Defaults to hour
     */
    public static int get(int getCode, TimeZone tz) {
        return get(getCode, getCalendar(tz));
    }

    public static int get(int getCode, Calendar cal) {
        switch (getCode) {
            case MINUTE_GET_CODE:
                return cal.get(Calendar.MINUTE);
            case SECOND_GET_CODE:
                return cal.get(Calendar.SECOND);
            case MILLISECOND_GET_CODE:
                return cal.get(Calendar.MILLISECOND);
            default: // hour
                return cal.get(Calendar.HOUR_OF_DAY);
        }
    }

    public static Calendar getCalendar(TimeZone tz) {
        // check if we already have the Calendar
        // for the requested TimeZone
        if (TZToC.containsKey(tz)) {
            return TZToC.get(tz);
        } else {
            // OK, we don't have it. Let's save it and
            // return it
            Calendar ret = Calendar.getInstance(tz);
            TZToC.put(tz, ret);
            return ret;
        }
    }

    // min < n < max
    // shift a value and keep it inside a range
    public static double sanitise(double n, double min, double max) {
        return (n - min + max) % max + min;
    }
}
